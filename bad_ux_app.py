from PyQt6.QtWidgets import (
    QApplication,
    QComboBox,
    QHBoxLayout,
    QLabel,
    QToolTip,
    QLineEdit,
    QMainWindow,
    QPushButton,
    QVBoxLayout,
    QWidget,
    QMessageBox,
)
from PyQt6.QtGui import QFont
from PyQt6.QtCore import Qt, QTimer, pyqtSignal

import random


class BadUXApp(QMainWindow):
    def __init__(self):
        super().__init__()

        # no titlebar -> annoy the user
        self.setWindowFlags(Qt.WindowType.FramelessWindowHint)

        # speed for moving window
        self.speed = 1
        self.dx = self.speed
        self.dy = self.speed

        # Timer for moving window
        self.timer = QTimer(self)
        self.timer.setInterval(int(1000 * 1 / 60))
        self.timer.timeout.connect(self.moveWindow)

        self.initUI()

    def initUI(self):
        self.setWindowTitle("Horrible UX")

        # annoy the user with a fixed window size
        self.setFixedSize(500, 500)

        # Set the central widget and layout
        central_widget = QWidget()
        layout = QVBoxLayout()

        self.close_button = HoverButton()
        # Wrong Text
        self.close_button.setText("Save")
        # Actually close the app
        self.close_button.clicked.connect(self.close)
        self.close_button.setMouseTracking(True)
        # Hover signals for starting window movement
        self.close_button.hoverStarted.connect(self.begin_move_window)
        self.close_button.hoverEnded.connect(self.end_move_window)
        self.close_button.setToolTip(
            "NOTE: This button will not save your data but instead just close the application without saving any changes!"
        )
        layout.addWidget(self.close_button)

        # Add line edit inputs with alternating labels and inputs, and horrible color contrast
        for i in range(4):
            row_layout = QHBoxLayout()
            if i % 2 == 0:
                label = QLabel("First Name:")
                label.setStyleSheet("color: red; background-color: yellow;")
                row_layout.addWidget(label)
                line_edit = QLineEdit()
                line_edit.setStyleSheet("color: white; background-color: blue;")
                row_layout.addWidget(line_edit)
                line_edit.textChanged.connect(self.onTextChanged)
            else:
                line_edit = QLineEdit()
                line_edit.setStyleSheet("color: white; background-color: green;")
                row_layout.addWidget(line_edit)
                label = QLabel("Last Name:")
                label.setStyleSheet("color: blue; background-color: pink;")
                row_layout.addWidget(label)
                line_edit.textChanged.connect(self.onTextChanged)
            layout.addLayout(row_layout)

        for i in range(4):
            row_layout = QHBoxLayout()
            if i % 2 == 0:
                label = QLabel("Email:")
                label.setStyleSheet("color: purple; background-color: orange;")
                row_layout.addWidget(label)
                line_edit = QLineEdit()
                line_edit.setStyleSheet("color: white; background-color: gray;")
                row_layout.addWidget(line_edit)
                line_edit.textChanged.connect(self.onTextChanged)
            else:
                line_edit = QLineEdit()
                line_edit.setStyleSheet("color: white; background-color: brown;")
                row_layout.addWidget(line_edit)
                label = QLabel("Age:")
                label.setStyleSheet("color: green; background-color: purple;")
                row_layout.addWidget(label)
                line_edit.textChanged.connect(self.onTextChanged)
            layout.addLayout(row_layout)

        # Add a label with bad color contrast and small font
        label = QLabel("This text should be invisible due to the bad color contrast.")
        label.setFont(QFont("Arial", 6))
        label.setStyleSheet("color: yellow; ")
        layout.addWidget(label)

        # Add an unnecessary widget
        unnecessary_widget = QComboBox()
        unnecessary_widget.addItem("Hmm")
        unnecessary_widget.addItem("...")
        unnecessary_widget.addItem("///")
        layout.addWidget(unnecessary_widget)

        # Add a text field with a small font
        text_field = QLineEdit()
        text_field.setPlaceholderText("Very tiny font")
        text_field.setFont(QFont("Arial", 6))
        layout.addWidget(text_field)

        # Add a confusing button
        confusing_button = QPushButton("Dont press me or bad things will happen")
        confusing_button.clicked.connect(self.onConfusingButtonClicked)
        layout.addWidget(confusing_button)

        # Set an unexpected background color
        central_widget.setStyleSheet("background-color: yellow;")
        central_widget.setLayout(layout)

        self.setCentralWidget(central_widget)

        # place window at the center of the screen
        self.moveWindowCenter()

    def closeEvent(self, event) -> None:
        """
        Shows a message box to annoy the user and prevent closing the app.
        """
        button = QMessageBox.question(
            self,
            "I have a question!",
            "Dont leave this beautiful application. Stay longer?",
        )
        # just ignore the answer and dont allow closing the app
        event.ignore()

    def onTextChanged(self, text):
        """
        Snaps the window to one of the corners of the screen or the center.
        """
        rd = random.randint(1, 5)
        if rd == 1:
            self.moveWindowCenter()
        elif rd == 2:
            self.moveWindowTopLeft()
        elif rd == 3:
            self.moveWindowTopRight()
        elif rd == 4:
            self.moveWindowBottomRight()
        elif rd == 5:
            self.moveWindowBottomLeft()
        return

    def onConfusingButtonClicked(self):
        """
        Starts moving the window and shows an annoying popup.
        """
        self.begin_move_window()
        self.showPopup()

    def showPopup(self):
        """
        Shows annoying popup.
        """
        # Show a popup with a bad message
        self.setWindowTitle("Nothing happened")
        popup = QLabel("Surprise! This message is totally unnecessary.", self)
        popup.setFont(QFont("Arial", 8))
        popup.setStyleSheet("color: red;")
        popup.move(150, 50)
        popup.show()

    def begin_move_window(self):
        self.timer.start()

    def end_move_window(self):
        self.timer.stop()

    def moveWindow(self):
        """
        Moves the window according to the dx and dy values and changes direction if the window is about to go out of bounds.
        """
        screen_geometry = QApplication.primaryScreen().availableGeometry()
        frame_geometry = self.frameGeometry()

        # New position
        new_x = frame_geometry.x() + self.dx
        new_y = frame_geometry.y() + self.dy

        # Check if the window is about to go out of bounds, change direction
        if new_x <= 0 or new_x + frame_geometry.width() >= screen_geometry.width():
            self.dx = -self.dx
        if new_y <= 0 or new_y + frame_geometry.height() >= screen_geometry.height():
            self.dy = -self.dy

        self.moveWindowInScreenBounds(self.dx, self.dy)

    def moveWindowInScreenBounds(self, dx, dy):
        """
        Moves the window by dx and dy, but keeps the window in the screen bounds.
        """
        screen_geometry = QApplication.primaryScreen().availableGeometry()
        frame_geometry = self.frameGeometry()
        # keep window in screen bounds
        new_x = max(
            0,
            min(
                screen_geometry.width() - frame_geometry.width(),
                frame_geometry.x() + dx,
            ),
        )
        new_y = max(
            0,
            min(
                screen_geometry.height() - frame_geometry.height(),
                frame_geometry.y() + dy,
            ),
        )
        # Move window to new position
        self.move(new_x, new_y)

    def moveWindowCenter(self):
        """
        Moves the window to the center of the screen.
        """
        screen_geometry = QApplication.primaryScreen().availableGeometry()
        frame_geometry = self.frameGeometry()
        x = (screen_geometry.width() - frame_geometry.width()) / 2
        y = (screen_geometry.height() - frame_geometry.height()) / 2
        self.move(int(x), int(y))

    def moveWindowTopLeft(self):
        """
        Moves the window to the top left corner of the screen.
        """
        self.move(0, 0)

    def moveWindowTopRight(self):
        """
        Moves the window to the top right corner of the screen.
        """
        screen_geometry = QApplication.primaryScreen().availableGeometry()
        frame_geometry = self.frameGeometry()
        x = screen_geometry.width() - frame_geometry.width()
        self.move(int(x), 0)

    def moveWindowBottomLeft(self):
        """
        Moves the window to the bottom left corner of the screen.
        """
        screen_geometry = QApplication.primaryScreen().availableGeometry()
        frame_geometry = self.frameGeometry()
        y = screen_geometry.height() - frame_geometry.height()
        self.move(0, int(y))

    def moveWindowBottomRight(self):
        """
        Moves the window to the bottom right corner of the screen.
        """
        screen_geometry = QApplication.primaryScreen().availableGeometry()
        frame_geometry = self.frameGeometry()
        x = screen_geometry.width() - frame_geometry.width()
        y = screen_geometry.height() - frame_geometry.height()
        self.move(int(x), int(y))


class HoverButton(QPushButton):
    """
    Button that emits signals when the mouse hovers over it.
    """

    hoverStarted = pyqtSignal()
    hoverEnded = pyqtSignal()

    def __init__(self):
        super().__init__()

    def enterEvent(self, event):
        super().enterEvent(event)
        # Show tooltip immediately
        QToolTip.showText(self.mapToGlobal(self.rect().center()), self.toolTip(), self)
        self.hoverStarted.emit()

    def leaveEvent(self, event):
        super().leaveEvent(event)
        self.hoverEnded.emit()


if __name__ == "__main__":
    app = QApplication([])

    badUXApp = BadUXApp()
    badUXApp.show()

    app.exec()
