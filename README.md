# Bad UX PyQt Application

## Overview

This project demonstrates an intentionally poorly designed PyQt application to highlight common user experience (UX) pitfalls. The application features bad color combinations, unnecessary widgets, very small font sizes, and an annoying moving window that makes it difficult for users to interact with the application.

## Features

- **No Title Bar**: The application window lacks a title bar to frustrate users.
- **Fixed Window Size**: The application has a fixed window size to limit the user’s control.
- **Horrible Color Combinations**: The application uses horrible color combinations for the inputs to make the interface unpleasant.
- **Unreadable Text**: The application contains labels with very small font sizes and poor color contrast, making the text difficult to read.
- **Unnecessary Widgets**: The application includes widgets that are not required, adding to the clutter and confusion.
- **Annoying Button**: A confusing button that, when clicked, displays a meaningless popup message and starts window movement.
- **Moving Window**: The main window moves continuously around the screen, bouncing off the edges, making it hard for users to focus on and use the application.
- **Window Movement on Text Input**: The window moves to a random corner or the center whenever text is changed in the line edits.
- **Window Movement on Hover**: The window starts moving when the save button is hovered over.
- **Disallowed Closing**: Prevents the user from closing the application by overriding the close event.
- **Misleading Save Button**: The "Save" button is misleading as it actually tries to close the application.

## Installation

To run this application, you need to have Python and PyQt6 installed. You can install PyQt6 using pip:

```sh
pip install PyQt6
```

## Running the Application

Save the provided code in a file named `bad_ux_app.py` and run it using the following command:

```sh
python bad_ux_app.py
```

## Code Structure

The application is structured into two main classes: `BadUXApp` and `HoverButton`.

### BadUXApp Class

This class defines the main window and its behavior.

#### Methods

- `__init__(self)`: Initializes the main window and starts the movement timer.
- `initUI(self)`: Sets up the UI components.
- `closeEvent(self, event)`: Prevents the user from closing the application by showing a message box.
- `onTextChanged(self, text)`: Moves the window to a random corner or the center when text changes.
- `onConfusingButtonClicked(self)`: Starts the window movement and shows an annoying popup.
- `showPopup(self)`: Displays an unnecessary popup message.
- `begin_move_window(self)`: Starts the window movement.
- `end_move_window(self)`: Stops the window movement.
- `moveWindow(self)`: Moves the window and changes direction if it hits the screen edges.
- `moveWindowInScreenBounds(self, dx, dy)`: Moves the window within screen bounds.
- `moveWindowCenter(self)`: Moves the window to the center of the screen.
- `moveWindowTopLeft(self)`: Moves the window to the top-left corner of the screen.
- `moveWindowTopRight(self)`: Moves the window to the top-right corner of the screen.
- `moveWindowBottomLeft(self)`: Moves the window to the bottom-left corner of the screen.
- `moveWindowBottomRight(self)`: Moves the window to the bottom-right corner of the screen.

### HoverButton Class

This class extends QPushButton to emit signals when the mouse hovers over it.

#### Methods

- `__init__(self)`: Initializes the button.
- `enterEvent(self, event)`: Emits `hoverStarted` signal when the mouse enters the button area.
- `leaveEvent(self, event)`: Emits `hoverEnded` signal when the mouse leaves the button area.

## License

This project is licensed under the MIT License.
